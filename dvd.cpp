#define WIN_HEIGHT 480
#define WIN_WIDTH 640
#define CCW 1
#define CW -1
#define COLLINEAR 0
#define PI 3.14159265
#define ERROR -9999
#define ZERO 10e-10
#define RED 1
#define BLACK 0
#define POSITIVE 1
#define NEGATIVE 0
#define REPEAT 1
#define NO_REPEAT 0
#define MIN_PRIORITY 0
#define MAX_PRIORITY 1
#define SITE_EVENT 1
#define CIRCLE_EVENT 0
#define NIL_EVENT 2
#define POINTSIZE 4

#include <iostream>
#include <cmath>
#include <vector>
#include <limits>
#include <string>
#include <typeinfo>
#include <ctime>
#include <map>
#include <algorithm>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

typedef int OutCode; 
const int INSIDE = 0; // 0000
const int LEFT = 1;   // 0001
const int RIGHT = 2;  // 0010
const int BOTTOM = 4; // 0100
const int TOP = 8;    // 1000

using namespace std;

// global vars
Display* dis;
Window win;
XSetWindowAttributes att;
GC gc, lineGc;
XEvent ev;

//	function definitions
template<typename T>
T Min(T i1, T i2){ return (i1 < i2)?i1:i2; }

template<typename T>
T Max(T i1, T i2){ return (i1 > i2)?i1:i2; }

template<typename T>
T getCenterY(T x1, T y1, T x2, T y2, T x3, T y3){
	double re1 = pow( x1, 2 ) + pow( y1, 2 ) - pow( x2, 2 ) - pow( y2, 2 );
	double re2 = pow( x1, 2 ) + pow( y1, 2 ) - pow( x3, 2 ) - pow( y3, 2 );

	double x = 0.0, y = 0.0;

	if( abs(x1 - x2) > ZERO ){
		double c1 = (x1 - x3)*re1/(x1 - x2);
		double c2 = 2.0 * (y1 - y2) * (x1 - x3) / (x1 - x2);
		double c3 = 2.0*y1 - 2.0*y3 - c2;

		if(abs(c3) < ZERO) abort();

		y = (re2 - c1)/c3;
		return y;
	}
	else if( abs(x1 - x3) > ZERO ){
		double c1 = re1 - (x1 - x2) * re2 / (x1 - x3);
		double c2 = 2.0*y1 - 2.0*y2 - 2.0 * (x1 - x2)*(y1 - y2)/(x1 - x3);

		if(abs(c2) < ZERO) abort();

		y = c1 / c2;
		return y;
	}
	else 
		abort(); 
}

template<typename T>
T getCenterX(T x1, T y1, T x2, T y2, T x3, T y3){
	double x = 0.0;

	if( abs(x1 - x2) > ZERO ){
		x = (x2 * x2 + y2 * y2 - x1 * x1 - y1 * y1 + 2.0*(y1 - y2) 
			* getCenterY(x1, y1, x2, y2, x3, y3))/(2.0*x2 - 2.0*x1);
	}
	else{
		x = (x3 * x3 + y3 * y3 - x1 * x1 - y1 * y1 + 2.0*(y1 - y3) 
			* getCenterY(x1, y1, x2, y2, x3, y3))/(2.0*x3 - 2.0*x1);
	}

	return x;
} 

template<typename T>
T getCenterX(T x1, T y1, T x2, T y2, T x3, T y3, T y){
	double x = 0.0;

	if( abs(x1 - x2) > ZERO ){
		x = (x2 * x2 + y2 * y2 - x1 * x1 - y1 * y1 + 2.0*(y1 - y2) * y)
			/(2.0*x2 - 2.0*x1);
	}
	else{
		x = (x3 * x3 + y3 * y3 - x1 * x1 - y1 * y1 + 2.0*(y1 - y3) * y)
			/(2.0*x3 - 2.0*x1);
	}

	return x;
}

template<typename T>
T getWipeX(T x1, T y1, T x2, T y2, T x3, T y3){
	T x = getCenterX(x1, y1, x2, y2, x3, y3);
	T y = getCenterY(x1, y1, x2, y2, x3, y3);
	T r = sqrt(pow((x - x1), 2.0) + pow((y - y1), 2.0));
		
	return x + r;
}

//calculate the arc
template<typename T>
inline T calcX(T y, T a, T b, T c){
	if( abs(a - c) < ZERO ) return ERROR;

	if(typeid(y).name() == typeid(a).name())
		return (y * y -2.0 * b * y + a * a + b * b - c * c)/(2.0 * (a - c));
	else{
		T tmp = static_cast<T>(y);
		return (tmp * tmp -2.0 * b * tmp + a * a + b * b - c * c)/(2.0 * (a - c));
	}
}

template<typename T>
inline T calcY(T x, T a, T b, T c, int upOrDown){
	if( abs(a - c) < ZERO ) return ERROR;

	if (1 == upOrDown)
		return (b + sqrt( (2.0*(a-c)*x) - (a*a) + (c*c) ));
	else
		return (b - sqrt( (2.0*(a-c)*x) - (a*a) + (c*c) ));
}


//	point definition
template<typename T>
class point {
public:
	T posX, posY;
	T speedX, speedY;
	bool moving;

	/*
	* constructor
	*/
	point();
	point(T x, T y);
	point(T x, T y, T dx, T dy);
	~point();

	/*
	* methods
	*/
	void move();
	short isCCW(point<T>*, point<T>*, point<T>*);
	short isCCW(point<T>&, point<T>&, point<T>&);
	short isCCW(point<T>*, point<T>*);

	/*
	* overload operators
	*/
	point<T>& operator++();
	point<T>& operator--();
	bool operator>(const point<T>&) const;
	bool operator>(const point<T>*) const;
	bool operator>=(const point<T>&) const;
	bool operator>=(const point<T>*) const;
	bool operator<(const point<T>&) const;
	bool operator<(const point<T>*) const;
	bool operator<=(const point<T>&) const;
	bool operator<=(const point<T>*) const;
	bool operator==(const point<T>&) const;
	bool operator==(const point<T>*) const;
	bool operator!=(const point<T>&) const;
	bool operator!=(const point<T>*) const;

	/*
	* transform from normal coordinates to X11
	*/
	point<T> trans();
};

template<typename T>
point<T> point<T>::trans(){
    T x = this->posX,
            y = this->posY,
            sx = this->speedX,
            sy = this->speedY,
            mv = this->moving;
    
	return point<T>(x, (WIN_HEIGHT - y));
}

/*	Operators overloading
*	++, --, >, >=, <, <=
*/
template<typename T>
bool point<T>::operator>(const point<T>& p) const{
	if( this->posX > p.posX ) return true;
	if( this->posX == p.posX && this->posY > p.posY ) return true;
	return false;
}

template<typename T>
bool point<T>::operator>(const point<T>* p) const{
	if( this->posX > p->posX ) return true;
	if( this->posX == p->posX && this->posY > p->posY ) return true;
	return false;
}

template<typename T>
bool point<T>::operator>=(const point<T>& p) const{
	if( this->posX > p.posX ) return true;
	if( this->posX == p.posX && this->posY >= p.posY ) return true;
	return false;
}

template<typename T>
bool point<T>::operator>=(const point<T>* p) const{
	if( this->posX > p->posX ) return true;
	if( this->posX == p->posX && this->posY >= p->posY ) return true;
	return false;
}

template<typename T>
bool point<T>::operator<(const point<T>& p) const{
	if( this->posX < p.posX ) return true;
	if( this->posX == p.posX && this->posY < p.posY ) return true;
	return false;
}

template<typename T>
bool point<T>::operator<(const point<T>* p) const{
	if( this->posX < p->posX ) return true;
	if( this->posX == p->posX && this->posY < p->posY ) return true;
	return false;
}

template<typename T>
bool point<T>::operator<=(const point<T>& p) const{
	if( this->posX < p.posX ) return true;
	if( this->posX == p.posX && this->posY <= p.posY ) return true;
	return false;
}

template<typename T>
bool point<T>::operator<=(const point<T>* p) const{
	if( this->posX < p->posX ) return true;
	if( this->posX == p->posX && this->posY <= p->posY ) return true;
	return false;
}

template<typename T>
point<T>& point<T>::operator++(){
	this->posX += this->speedX;
	this->posY += this->speedY;
	return *this;
}

template<typename T>
point<T>& point<T>::operator--(){
	this->posX -= this->speedX;
	this->posY -= this->speedY;
	return *this;
}

template<typename T>
bool point<T>::operator==(const point<T>& p) const{
	return this->posX == p.posX
		&& this->posY == p.posY;
}

template<typename T>
bool point<T>::operator==(const point<T>* p) const{
	return this->posX == p->posX
		&& this->posY == p->posY;
}

template<typename T>
bool point<T>::operator!=(const point<T>* p) const{
	return this->posX != p->posX 
		|| this->posY != p->posY;
}

template<typename T>
bool point<T>::operator!=(const point<T>& p) const{
	return this->posX != p.posX
		|| this->posY != p.posY;
}

template<typename T>
ostream& operator<<(ostream& os, point<T> p){
	return cout<<"(x:"<<p.posX<<", y:"<<p.posY<<")";
}

template<typename T>
point<T>::point():posX(0), posY(0), speedX(0), speedY(0) {}

template<typename T>
point<T>::point(T x, T y):posX(x), posY(y), speedX(0), speedY(0) {}

template <typename T>
point<T>::point(T x, T y, T dx, T dy): posX(x), posY(y), speedX(dx), speedY(dy)	{}

template <typename T>
point<T>::~point(){
	this->posX = 0;
	this->posY = 0;
	this->speedX = 0;
	this->speedY = 0;
}

template <typename T>
void point<T>::move(){
    if(this->posX + this->speedX > WIN_WIDTH){
        this->posX = 2 * WIN_WIDTH - this->posX - this->speedX;
        this->speedX *= -1.0;
    }
    else if(this->posX + this->speedX < 0){
        this->posX = abs(this->posX + this->speedX);
        this->speedX *= -1.0;
    }
    else
        this->posX += this->speedX;
    
    if(this->posY + this->speedY > WIN_HEIGHT){
        this->posY = 2 * WIN_HEIGHT - this->posY - this->speedY;
        this->speedY *= -1.0;
    }
    else if(this->posY + this->speedY < 0){
        this->posY = abs(this->posY + this->speedY);
        this->speedY *= -1.0;
    }
    else
        this->posY += this->speedY;
}

template<typename T>
short point<T>::isCCW(point<T>* a, point<T>* b, point<T>* c){
	T area = (b->posX - a->posX)*(c->posY - a->posY) - (b->posY - a->posY)*(c->posX - a->posX);
	if( area > 0 ) return CCW;
	if( area < 0 ) return CW;
	return COLLINEAR;
}

template<typename T>
short point<T>::isCCW(point<T>& a, point<T>& b, point<T>& c){
	T area = (b.posX - a.posX)*(c.posY - a.posY) - (b.posY - a.posY)*(c.posX - a.posX);
	if( area > 0 ) return CCW;
	if( area < 0 ) return CW;
	return COLLINEAR;
}

template<typename T>
short point<T>::isCCW(point<T>* n, point<T>* nn){
	return this->isCCW(this, n, nn);
}

//	function which require definition of point
template<typename T>
point<T> getCenter(point<T>* p1, point<T>* p2, point<T>* p3){
	double y = getCenterY(p1->posX, p1->posY, p2->posX, p2->posY, p3->posX, p3->posY),
		x = getCenterX(p1->posX, p1->posY, p2->posX, p2->posY, p3->posX, p3->posY, y);

	return point<T>(x, y);
}

template<typename T>
T getBoundingWipe(point<T>* p1, point<T>* p2){
	T x1 = p1->posX,
		y1 = p1->posY,
		x2 = p2->posX,
		y2 = p2->posY;
	T x3, y3;

	if(x1 == x2){
		x3 = WIN_WIDTH;
		y3 = (y1 + y2)/2.0;

		return sqrt(pow((x3-x1), 2.0) + pow((y3-y1),2.0)) + WIN_WIDTH;
	}
	else if(y1 == y2){
		x3 = (x1 + x2)/2.0;
		if(y1 < (WIN_HEIGHT/2.0))
			y3 = WIN_HEIGHT;
		else
			y3 = 0;

		return x3 + sqrt(pow((x3-x1), 2.0) + pow((y3-y1),2.0));
	}
	else {
		T k = (y1 - y2) / (x1 - x2);
		k *= -1;

		T c = (y1 + y2)/2.0 - k * (x1 + x2) / 2.0;

		if( 0 <= (k*WIN_WIDTH + c) && WIN_HEIGHT >= (k*WIN_WIDTH + c) ){
			x3 = WIN_WIDTH;
			y3 = (k*WIN_WIDTH + c);
		}
		else if( (-1 * c/k) > ((WIN_HEIGHT - c)/k)){
			x3 = (-1 * c/k);
			y3 = 0;
		}
		else{
			x3 = (WIN_HEIGHT - c)/k;
			y3 = WIN_HEIGHT;
		}

		return x3 + sqrt(pow((x3-x1), 2.0) + pow((y3-y1),2.0));
	}
}

/*
*	given 2 site point, according to the space relation
*	calculate the angle of the halfEdge(belongs to selfSite)'s angle with y = 0 
*/
template<typename T>
T calcAngle(point<T>* selfSite, point<T>* nbSite){
	if(abs( nbSite->posX - selfSite->posX && nbSite->posY == selfSite->posY) < ZERO) {
		return 0.0;
	}

	if(abs( selfSite->posX - nbSite->posX)<ZERO ){			
		if(selfSite->posY > nbSite->posY)
			return  0.0;
		else
			return  180.0;
	}

	if(abs(selfSite->posY - nbSite->posY) <ZERO){
		if(selfSite->posX > nbSite->posX)
			return  270.0;
		else
			return  90.0;
	}

	T k = (nbSite->posY - selfSite->posY)/(nbSite->posX - selfSite->posX);
	//now x1 != x2 && y1 != y2;
	if(selfSite->posX > nbSite->posX){
		if(selfSite->posY > nbSite->posY){
			//4th quadrant
			return  270.0 + (atan(abs(k))*180.0/PI);
		}
		else{
			//3nd quadrant
			return  270.0 - (atan(abs(k))*180.0/PI);
		}
	}
	else{
		if(selfSite->posY > nbSite->posY){
			//1st quadrant
			return  90.0 - (atan(abs(k))*180.0/PI);
		}
		else{
			//2nd quadrant
			return  90.0 + (atan(abs(k))*180.0/PI);
		}
	}
	return  0.0;
}

template <typename T>
T angleFromSiteToCenter(point<T>* p, point<T>* center){
    if(abs(p->posX - center->posX)<ZERO ){
        if(p->posY > center->posY) return 90.0;
        return 270.0;
    }
    
    if(abs(p->posY - center->posY)<ZERO ){
        if(p->posX > center->posX) return 0;
        return 180.0;
    }
    
    double k = abs((p->posY - center->posY)/(p->posX - center->posX));
    
    if(p->posX > center->posX){
        if(p->posY > center->posY){
            return (atan(k)*180.0/PI);
        }
        else{
            return 360.0 - (atan(k)*180.0/PI);
        }
    }
    else{
        if(p->posY > center->posY){
            return 180.0 - (atan(k)*180.0/PI);
        }
        else{
            return 180.0 + (atan(k)*180.0/PI);
        }
    }
}

/*
*	use this function to calculate the inersection point of 2 arcs
*	@requirement
*	1.	coordinates of 2 site points
*	2.	the order of the 2 site points
*	those 2 conditions will determine 1 and only 1 intersection point
*/
template<typename T>
void calcIntersectionPoint(point<T>* up, point<T>* down, point<T>* inter, T wipe){
	if(up == down) return;

	if(up->posX > wipe || down->posX > wipe) return;

	if(up->posX == down->posX && up->posX == wipe){
		T maxVal = std::numeric_limits<T>::max();
		T minVal = -1.0 * maxVal;
        //T minVal = -1.0;
		inter->posX = minVal;
		inter->posY = (up->posY + down->posY)/2.0;
		return;
	}

	T x = 0, y = 0;
	T a = 0, b = 0, c = 0;

	if(up->posX == wipe){
		y = up->posY;
		a = down->posX;
		b = down->posY;
		c = wipe;

		x = ((y - b)*(y - b) + a*a - c*c)/(2.0*(a - c));
		inter->posX = x;
		inter->posY = y;
		return;
	}

	if(down->posX == wipe){
		y = down->posY;
		a = up->posX;
		b = up->posY;
		c = wipe;

		x = ((y - b)*(y - b) + a*a - c*c)/(2.0*(a - c));
		inter->posX = x;
		inter->posY = y;
		return;
	}

	T
		a1 = up->posX,
		b1 = up->posY,
		c1 = wipe,
		a2 = down->posX,
		b2 = down->posY,
		c2 = c1;

	T 
		coef1 = (a1 - c1) / (a2 - c2),
		coef2 = a1 * a1 + b1 * b1 - c1 * c1,
		coef3 = a2 * a2 + b2 * b2 - c2 * c2;

	a = 1.0 - coef1,
	b = 2.0 * (b2 * coef1 - b1),
	c = coef2 - coef1 * coef3;

	if( 0 == a ){	//this means the 2 sites are with the same x coordiante
		if(0 == b)	//this means the 2 sites coincide.
			return;
		else{
			y = -c / b;
			x = calcX<T>(y, up->posX, up->posY, wipe);

			inter->posX = x;
			inter->posY = y;
		}
	}
	else{
		T 
			yup = (-b + sqrt((b * b - 4 * a * c)))/(2.0 * a),
			ydown = (-b - sqrt((b * b - 4 * a * c)))/(2.0 * a);

		if( up->posX > down->posX ){
			y = Min<T>(yup, ydown);
			x = calcX<T>(y, up->posX, up->posY, wipe);
			inter->posX = x;
			inter->posY = y;
		}
		else{
			y = Max<T>(yup, ydown);
			x = calcX<T>(y, up->posX, up->posY, wipe);
			inter->posX = x;
			inter->posY = y;
		}	
	}
}

//	half edge definition
template<typename T>
class halfEdge{
public:
	typedef point<T> _point;
	typedef halfEdge<T> _halfEdge;

	_point* site;
	T key;
	_point *start, *end;
	_halfEdge *twin;

	halfEdge();
	halfEdge(_point* s, _point *st, _point *ed): site(s), start(st), end(ed) {}
	halfEdge(_point* s, T k, _point *st, _point *ed): site(s), key(k), start(st), end(ed) {}
	halfEdge(_point* s, T k, _point *st, _point *ed, halfEdge<T> *t): site(s), key(k), start(st), end(ed), twin(t) {}
	void calcKey();
    _halfEdge clipByWindow();
	bool operator>(const halfEdge<T>& he) const {return this->key > he.key; }
	bool operator>=(const halfEdge<T>& he) const {return this->key >= he.key; }
	bool operator<(const halfEdge<T>& he) const {return this->key < he.key; }
	bool operator<=(const halfEdge<T>& he) const {return this->key <= he.key; }
	bool operator==(const halfEdge<T>& he) const {return this->key == he.key && this->site == he.site && this->twin == he.twin; }
};

template<typename T>
ostream& operator<<(ostream& os, halfEdge<T> he){
	return cout<<"HalfEdge (key:"<< he.key <<")"<< * he.start<<" -> "<<* he.end<<endl;
}

template<typename T>
halfEdge<T>::halfEdge():site(0), key(0), start(0), end(0), twin(0) {}

template<typename T>
void halfEdge<T>::calcKey(){
	this->key = calcAngle<T>(this->site, this->twin->site);
}

//	definition of break point class
template<typename T>  //T is a numeric type for the coordinates
class breakPoint {
public:
	typedef point<T> _point;

	_point *vertex; //need to free this memory
	_point *upSite, *downSite;

	breakPoint();
	breakPoint(_point*, _point*, _point*);
	~breakPoint();

	//	methods
	void updateCoordinates(T wipe);
	bool operator>(const breakPoint<T> bk) const {  //  not quite sure about this, could be a bug
			
		if(this->downSite == bk.upSite && this->upSite == bk.downSite){
			return (this->downSite->posX > bk.downSite->posX);
		}
		else if(abs(this->vertex->posY - bk.vertex->posY)<ZERO){
			return angleFromSiteToCenter(this->upSite, this->vertex) < angleFromSiteToCenter(bk.upSite, bk.vertex);
		}
		else{
			return (this->vertex->posY > bk.vertex->posY);
		}
	}

	bool operator>=(const breakPoint<T> bk) const { 
		return (*this > bk);
	}

	bool operator<(const breakPoint<T> bk) const { 
		if(this->downSite == bk.upSite && this->upSite == bk.downSite){
			return (this->downSite->posX < bk.downSite->posX);
		}
		else if(abs(this->vertex->posY - bk.vertex->posY)<ZERO){
			return angleFromSiteToCenter(this->upSite, this->vertex) > angleFromSiteToCenter(bk.upSite, bk.vertex);
		}
		else{
			return (this->vertex->posY < bk.vertex->posY);
		}
	}

	bool operator<=(const breakPoint<T> bk) const { 
		return (*this < bk);
	}

	bool operator==(const breakPoint<T> bk) const { 
		return (this->upSite == bk.upSite && this->downSite == bk.downSite);
	}

	bool operator!=(const breakPoint<T> bk) const {
		return (this->upSite != bk.upSite || this->downSite != bk.downSite);
	}
}; 

//constructor
template<typename T>
breakPoint<T>::breakPoint():vertex(0), upSite(0), downSite(0) {}

template<typename T>
breakPoint<T>::breakPoint(_point *vT, _point *upS, _point *downS) : vertex(vT), upSite(upS), downSite(downS) {}	


template<typename T>
breakPoint<T>::~breakPoint(){}

template<typename T>
void breakPoint<T>::updateCoordinates(T wipe){
	calcIntersectionPoint<T>(this->upSite, this->downSite, this->vertex, wipe);
}

//	definition of genericNode
template<typename T>
class redBlackTree;

template<typename T>
class genericNode{
public:
	typedef genericNode<T> _genericNode;
	typedef redBlackTree<T> _redBlackTree;
	/*
	*	some common feature of the node 
	*	3 points : right, left, parent
	*	color : bool (1 or true for red/0 or false for black)
	*/
	_genericNode *right, *left, *parent;
	bool color;
	T key; //	maybe I should use some generic property rather than y (coordinate)
	_redBlackTree* host;

	/*
	*	constructor
	*/
	genericNode();
	~genericNode();
	genericNode(T key);
	genericNode( _genericNode*, _genericNode*, _genericNode*, T); 

	/*
	*	methods
	*/
	void init();
	_genericNode* next();
	_genericNode* pre();
};
		
template<typename T>
genericNode<T>* genericNode<T>::next(){
	return this->host->successor(this);
}

template<typename T>
genericNode<T>* genericNode<T>::pre(){
	return this->host->predecessor(this);
}

template<typename T>
void genericNode<T>::init(){
	this->parent = 0; this->left = 0; this->right = 0;
	this->color = BLACK;
}

//	constructors of genericNode class.
template<typename T>
genericNode<T>::genericNode():right(0), left(0), parent(0), color(BLACK){}

template<typename T>
genericNode<T>::genericNode(genericNode<T>* parent, genericNode<T>* left, genericNode<T>* right, T key):
parent(parent), left(left), right(right), color(BLACK), key(key)
{}

template<typename T>
genericNode<T>::genericNode(T key):right(0), left(0), parent(0), color(BLACK), key(key){}

template<typename T>
genericNode<T>::~genericNode(){
	this->color = BLACK;
	this->right = 0; this->left = 0; this->parent = 0;
}

//	definition of red black tree
template<typename T>  // T is same as the T of genericNode, it can be int, double, pointer or object
class redBlackTree{
public:
	typedef genericNode<T>* _iterator;
	typedef genericNode<T> _genericNode;

	_iterator root;
	static _iterator NIL; //all the node, if parent or left or right pointer is null, then it must poit to this->NIL
	int size;
	bool repeat;

	//friends
	friend class genericNode<T>;

	redBlackTree();
	~redBlackTree();

	void clear(_iterator);
	void clear();

	/*
	*	some tree operations
	*	left rotate + right rotate
	*	insert + delte 
	*/
			
	_iterator insert(T key);	
	redBlackTree<T>& remove(_iterator node);
	redBlackTree<T>& remove(T key);			
	_iterator search(T key, _iterator subtree);
	T* search(T key);
	_iterator nextContainer(T key);
	_iterator preContainer(T key);
	T popMin();
	T popMax();
	_iterator begin();
	_iterator end();
	_iterator last();
	T front();
	T back();
	T predecessor(T);
	T successor(T);

	T operator[](int index);

	/*
	*	utility functions
	*/			
	void treeToVector(vector<T>&, _iterator);
		
	/*
	*	tree operations
	*/
	void leftRotate(_iterator node);
	void rightRotate(_iterator node);
	void insertFixup(_iterator node);
	void transplant(_iterator, _iterator);
	void removeFixup(_iterator node);

	/*
	*	utilities
	*/
	_iterator min(_iterator);
	_iterator min();
	_iterator max(_iterator);
	_iterator max();
	_iterator predecessor(_iterator);			
	_iterator successor(_iterator);			
	void resetNil();
};		

/*
*	tree operation implementations
*/
template<typename T>
genericNode<T>* redBlackTree<T>::search(T key, genericNode<T>* subtree){
	if(this->NIL == subtree || 0 == subtree) return 0;
	if(key == subtree->key){
		//cout<<"hit"<<endl;
		return subtree;
	}
	else if(key > subtree->key){
		//cout<<"right"<<endl;
		return this->search(key, subtree->right);
	}
	else {
		//cout<<"left"<<endl;
		return this->search(key, subtree->left);
	}
}

template<typename T>
T* redBlackTree<T>::search(T key){
	if( 0 == this->search(key, this->root) ) 
		return 0;
	else
		return &this->search(key, this->root)->key;
}

template<typename T>
genericNode<T>* redBlackTree<T>::nextContainer(T key){
	return this->successor(this->search(key, this->root));
}

template<typename T>
genericNode<T>* redBlackTree<T>::preContainer(T key){
	return this->predecessor(this->search(key, this->root));
}

template<typename T>
void redBlackTree<T>::leftRotate(genericNode<T>* node){
	if (1 >= this->size )
		return;

	if (this->NIL == node->right)
		return;

	_iterator tmpRight = node->right;
	node->right = tmpRight->left;

	if (tmpRight->left != this->NIL){
		tmpRight->left->parent = node;
	}

	tmpRight->parent = node->parent;

	if (node->parent == this->NIL)
		this->root = tmpRight;
	else if (node == node->parent->left)
		node->parent->left = tmpRight;
	else
		node->parent->right = tmpRight;

	tmpRight->left = node;
	node->parent = tmpRight;

	tmpRight = NULL;
}

template<typename T>
void redBlackTree<T>::rightRotate(genericNode<T>* node){
	if (1 >= this->size )
		return;

	if (this->NIL == node->left)
		return;

	_iterator tmpLeft = node->left;
	node->left = tmpLeft->right;

	if (tmpLeft->right != this->NIL){
		tmpLeft->right->parent = node;
	}

	tmpLeft->parent = node->parent;

	if (node->parent == this->NIL)
		this->root = tmpLeft;
	else if (node == node->parent->left)
		node->parent->left = tmpLeft;
	else
		node->parent->right = tmpLeft;

	tmpLeft->right = node;
	node->parent = tmpLeft;

	tmpLeft = NULL;
}

template<typename T>
genericNode<T>* redBlackTree<T>::insert(T key){
	_iterator node = new _genericNode();	//	(genericNode<T>*) malloc (sizeof(genericNode<T>));
	node->init();
	node->key = key;
	node->parent = this->NIL;
	node->left = this->NIL;
	node->right = this->NIL;
	node->color = RED;
	node->host = this;

	if (0 == this->size){
		this->root = node;
		this->root->color = BLACK;
		this->size = 1;
		return node;
	}

	genericNode<T> *tmp = NULL, *iter = NULL; //tmp:y, iter:x
	tmp = this->NIL;
	iter = this->root;

	while(this->NIL != iter){
		tmp = iter;
		if(NO_REPEAT == this->repeat){
			if(node->key == iter->key)
				return this->NIL;
		}

		if(node->key > iter->key)
			iter = iter->right;
		else
			iter = iter->left;
	}

	node->parent = tmp;
	if (node->key > tmp->key){
		tmp->right = node;
	}
	else{
		tmp->left = node;
	}

	this->size++;
	this->insertFixup(node);

	tmp = NULL;
	iter = NULL;

	return node;		
}

template<typename T>
void redBlackTree<T>::insertFixup(genericNode<T>* node){
	_iterator tmp = this->NIL;
	if(1 >= this->size) return;
	if(this->root == node->parent) return;

	while(RED == node->parent->color){	
		if(node->parent == node->parent->parent->left){
				
			tmp = node->parent->parent->right;

			if(RED == tmp->color){
				node->parent->color = BLACK;					
				node->parent->parent->color = RED;					
				tmp->color = BLACK;
				node = node->parent->parent;
			}
			else if(node == node->parent->right){
				node = node->parent;
				this->leftRotate(node);

				if(RED == node->color){
					node->parent->color = BLACK;
					node->parent->parent->color = RED;
					this->rightRotate(node->parent->parent);
				}
			}
			else {
				node->parent->color = BLACK;				
				node->parent->parent->color = RED;					
				this->rightRotate(node->parent->parent);
				node = node->parent;
				break;
			}
		}
		else{
			tmp = node->parent->parent->left;

			if(RED == tmp->color){
				node->parent->color = BLACK;					
				node->parent->parent->color = RED;					
				tmp->color = BLACK;
				node = node->parent->parent;
			}
			else if(node == node->parent->left){
				node = node->parent;
				this->rightRotate(node);

				if(RED == node->color){
					node->parent->color = BLACK;
					node->parent->parent->color = RED;
					this->leftRotate(node->parent->parent);
				}
			}
			else {
				node->parent->color = BLACK;						
				node->parent->parent->color = RED;					
				this->leftRotate(node->parent->parent);
				node = node->parent;
				break;
			}
		}
	}

	if(this->root->color == RED)
		this->root->color = BLACK;

	tmp = NULL;
}

template<typename T>
void redBlackTree<T>::transplant(genericNode<T>* target, genericNode<T>* replacement){
	if(this->NIL == target) return;

	if(target->parent == this->NIL)
		this->root = replacement;
	else if(target == target->parent->left)
		target->parent->left = replacement;
	else
		target->parent->right = replacement;

	replacement->parent = target->parent;
}	

template<typename T>
redBlackTree<T>& redBlackTree<T>::remove(genericNode<T>* node){
	if(this->NIL == node || 0 == node){ 
		return *this;
	}

	_iterator iter = node;
	bool iterOriginalColor = iter->color;
	_iterator mark = this->NIL;

	if( node->left == this->NIL ){
		mark = node->right;
		this->transplant(node, node->right);
	}
	else if( node->right == this->NIL ){
		mark = node->left;
		this->transplant(node, node->left);
	}
	else{
		iter = this->min(node->right);
		iterOriginalColor = iter->color;
		mark = iter->right;

		if( iter->parent == node ){
			mark->parent = iter;
		}
		else{
			this->transplant(iter, iter->right);
			iter->right = node->right;
			iter->right->parent = iter;
		}
		this->transplant(node, iter);
		iter->left = node->left;
		iter->left->parent = iter;
		iter->color = node->color;
	}		

	if( iterOriginalColor == BLACK )
		this->removeFixup(mark);

	node = NULL;
	mark = NULL;

	this->resetNil();
	delete(node);
	this->size --;
	return *this;
}

template<typename T>
redBlackTree<T>& redBlackTree<T>::remove(T key){
	_iterator node =  this->search(key, this->root);

	if(this->NIL == node || 0 == node){
		node = NULL;
		return *this;
	}
	else{
		return this->remove(node);
	}
}

template<typename T>
void redBlackTree<T>::removeFixup(genericNode<T>* node){
	_iterator iter = this->NIL;

	while(this->root != node && node->color == BLACK){
		if( node == node->parent->left ){
			iter = node->parent->right;
			if( iter->color == RED ){
				iter->color = BLACK;
				node->parent->color = RED;
				this->leftRotate(node->parent);
				iter = node->parent->right;
			}

			if( iter->left->color == BLACK && iter->right->color == BLACK ){
				iter->color = RED;
				node = node->parent;
			}
			else{ 
				if( iter->right->color == BLACK ){
					iter->left->color = BLACK;
					iter->color = RED;
					this->rightRotate(iter);
					iter= node->parent->right;
				}

				iter->color = node->parent->color;
				node->parent->color = BLACK;
				iter->right->color = BLACK;
				this->leftRotate(node->parent);
				node = this->root;
			}
		}
		else{
			iter = node->parent->left;
			if( iter->color == RED ){
				iter->color = BLACK;
				node->parent->color = RED;
				this->rightRotate(node->parent);
				iter = node->parent->left;
			}

			if( iter->right->color == BLACK && iter->left->color == BLACK ){
				iter->color = RED;
				node = node->parent;
			}
			else{ 
				if( iter->left->color == BLACK ){
					iter->right->color = BLACK;
					iter->color = RED;
					this->leftRotate(iter);
					iter= node->parent->left;
				}

				iter->color = node->parent->color;
				node->parent->color = BLACK;
				iter->left->color = BLACK;
				this->rightRotate(node->parent);
				node = this->root;
			}
		}
	}

	node->color = BLACK;
	iter = NULL;
}

/*
*	utility functions
*/
template<typename T>
genericNode<T>* redBlackTree<T>::min(genericNode<T>* node){
	if(this->NIL == node) return node;

	_iterator iter = node;
	while(this->NIL != iter->left){
		iter = iter->left;
	}

	return iter;
}

template<typename T>
genericNode<T>* redBlackTree<T>::min(){
	return this->min(this->root);
}

template<typename T>
T redBlackTree<T>::popMin(){
	_iterator min = this->min(this->root);

	if(this->NIL == min) abort();

	T key = min->key;
	this->remove(min);
	min = NULL;

	return key;
}

template<typename T>
genericNode<T>* redBlackTree<T>::max(genericNode<T>* node){
	if(this->NIL == node || 0 == node) return this->NIL;

	_iterator iter = node;
	while(this->NIL != iter->right)
		iter = iter->right;

	return iter;
}

template<typename T>
genericNode<T>* redBlackTree<T>::max(){
	return this->max(this->root);
}

template<typename T>
T redBlackTree<T>::popMax(){
	_iterator max = this->max(this->root);

	if(this->NIL == max) abort();

	T key = max->key;
	this->remove(max);
	max = NULL;

	return key;
}

template<typename T>
genericNode<T>* redBlackTree<T>::begin(){
	return this->min();
}

template<typename T>
genericNode<T>* redBlackTree<T>::end(){
	return this->NIL;
}

template<typename T>
genericNode<T>* redBlackTree<T>::last(){
	return this->max();
}

template<typename T>
T redBlackTree<T>::front(){
	if(0 == this->size) {
		abort();
	}
	else
		return this->min()->key;
}

template<typename T>
T redBlackTree<T>::back(){
	if(0 == this->size) {
		abort();
	}
	else
		return this->max()->key;
}

template<typename T>
T redBlackTree<T>::operator[](int index){
	if( index < 0 || this->size <= index ) {				
		abort();
	}

	if(index <= (this->size / 2)){
		_iterator iter = this->min(this->root);
		for(int i=0; i<index; i++)
			iter = this->successor(iter);
		return iter->key;
	}
	else{
		_iterator iter = this->max(this->root);
		for(int i=0; i<this->size-1-index; i++)
			iter = this->predecessor(iter);
		return iter->key;
	}
}

template<typename T>
genericNode<T>* redBlackTree<T>::successor(genericNode<T>* node){
	if(0 == node || this->NIL == node) return this->NIL;

	if(this->NIL != node->right) 
		return this->min(node->right);

	genericNode<T> *iter = node,
		*ret = node->parent;

	while(ret != this->NIL && iter == ret->right){
		iter = ret;
		ret = ret->parent;
	}

	iter = NULL;

	return ret;
}

template<typename T>
T redBlackTree<T>::successor(T key){
	if(this->NIL != this->successor( this->search(key, this->root) ) )
		return this->successor( this->search(key, this->root) )->key;
	else
		abort();
}

template<typename T>
genericNode<T>* redBlackTree<T>::predecessor(genericNode<T>* node){
	if(0 == node || this->NIL == node) return this->NIL;

	if(this->NIL != node->left)
		return this->max(node->left);

	genericNode<T> *iter = node,
		*mark = node->parent;

	while(mark != this->NIL && iter == mark->left){
		iter = mark;
		mark = mark->parent;
	}

	iter = NULL;

	return mark;
}

template<typename T>
T redBlackTree<T>::predecessor(T key){
	_iterator node =  this->search(key, this->root);
	if(this->NIL == node)
		abort();

	return this->predecessor(node)->key;
}

template<typename T>
void redBlackTree<T>::clear(genericNode<T>* node){
	while(0 != this->size)
		this->remove(this->root);
}

template<typename T>
void redBlackTree<T>::clear(){
	this->clear(this->root);
}

template<typename T>
redBlackTree<T>::~redBlackTree(){
	this->size = 0;
	this->root = 0;
	this->clear();
	free(this->NIL);
}

template<typename T>
genericNode<T>* redBlackTree<T>::NIL = (genericNode<T>*) malloc ( sizeof(genericNode<T>) );

template<typename T>
redBlackTree<T>::redBlackTree():root(0), size(0){
	this->repeat = NO_REPEAT;
	this->NIL->init();
	this->NIL->left = this->NIL;
	this->NIL->right = this->NIL;
	this->NIL->parent = this->NIL;
	this->NIL->color = BLACK;
	this->NIL->host = this;
}

template<typename T>
void redBlackTree<T>::resetNil(){
	this->NIL->init();
	this->NIL->left = this->NIL;
	this->NIL->right = this->NIL;
	this->NIL->parent = this->NIL;
	this->NIL->color = BLACK;
}	

template<typename T>
void redBlackTree<T>::treeToVector(vector<T>& vec, genericNode<T>* node){
	if(this->NIL == node) return;

	this->treeToVector(vec, node->left);
	vec.push_back(node->key);
	this->treeToVector(vec, node->right);
}

//	definition of priority queue
template<typename T>
class priorityQueue : public redBlackTree<T> {
public:
	priorityQueue(bool);
	priorityQueue();

	//	methods
	bool mode;
	T pop();
	T front();
	T back();
};

template<typename T>
priorityQueue<T>::priorityQueue(bool mode) { this->mode = mode; }

template<typename T>
priorityQueue<T>::priorityQueue() { this->mode = MIN_PRIORITY; }

template<typename T>
T priorityQueue<T>::back(){
	if(this->mode == MAX_PRIORITY)
		return this->begin()->key;
	else
		return this->last()->key;
}

template<typename T>
T priorityQueue<T>::front(){
	if(this->mode == MAX_PRIORITY)
		return this->last()->key;
	else
		return this->begin()->key;
}

template<typename T>
T priorityQueue<T>::pop(){
	if(this->mode == MAX_PRIORITY)
		return this->popMax();
	else
		return this->popMin();
}

//	definition of locker [memory for points]
template<typename T>
class lockers{
public:	
	typedef vector<T*> _addressVector;
	typedef typename _addressVector::iterator _addressVector_iter;
	int size;

	lockers();
	~lockers();

	//	methods
	T* deposit(T stuff);
	T* operator[](int index);

	_addressVector memory;
};

template<typename T>
lockers<T>::lockers(){
	this->size = 0;
}

template<typename T>
lockers<T>::~lockers(){
	if(0 != this->memory.size()){
		for(_addressVector_iter iter = this->memory.begin();
			iter != this->memory.end();
			++ iter)
		{
			free(*iter);
		}
	}
	this->size = 0;
}

template<typename T>
T* lockers<T>::operator[](int index){
	if(index >= (int)this->memory.size())
		abort();

	return this->memory[index];
}

template<typename T>
T* lockers<T>::deposit(T stuff){
	T* tmpPtr = (T*)malloc(sizeof(T));
	*tmpPtr = stuff;
	this->memory.push_back(tmpPtr);
	this->size++;

	return tmpPtr;
}

//	definition of Event
template<typename T>	//	T in here is a numeric type {short, int, float, double, long ...}
class Event{
public:
	typedef point<T> _point;
	typedef breakPoint<T> _breakPoint;

	short eventType;
	T wipe;
	_point* p;
	_breakPoint *up, *down;

	Event():eventType(NIL_EVENT), wipe(0), up(0), down(0) {}
	Event(short et, T wp):eventType(et), wipe(wp), p(0), up(0), down(0){}
	Event(short et, T wp, _point* pt):eventType(et), wipe(wp), p(pt), up(0), down(0){}
	Event(short	et,	T wp,_breakPoint *upBP,_breakPoint *downBP): eventType(et), wipe(wp), up(upBP), down(downBP){}
		
	bool operator>(const Event<T>& e) const  { 
		if( this->wipe == e.wipe ){
			if(CIRCLE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return true;
			}
			else if(CIRCLE_EVENT == this->eventType &&
				CIRCLE_EVENT == e.eventType)
			{
				return this->up->vertex->posY > e.up->vertex->posY;
			}
			else if(SITE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return this->p->posY > e.p->posY;
			}
			else
				return false;
		}
		else
			return this->wipe > e.wipe; 
	}

	bool operator>=(const Event<T>& e) const {
		if( this->wipe == e.wipe ){
			if(CIRCLE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return true;
			}
			else if(CIRCLE_EVENT == this->eventType &&
				CIRCLE_EVENT == e.eventType)
			{
				return this->up->vertex->posY >= e.up->vertex->posY;
			}
			else if(SITE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return this->p->posY >= e.p->posY;
			}
			else
				return false;
		}
		else
			return this->wipe > e.wipe; 
	}

	bool operator<(const Event<T>& e) const  { 
		if( this->wipe == e.wipe ){
			if(CIRCLE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return false;
			}
			else if(CIRCLE_EVENT == this->eventType &&
				CIRCLE_EVENT == e.eventType)
			{
				return this->up->vertex->posY < e.up->vertex->posY;
			}
			else if(SITE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return this->p->posY < e.p->posY;
			}
			else
				return true;
		}
		else
			return this->wipe < e.wipe; 
	}

	bool operator<=(const Event<T>& e) const  { 
		if( this->wipe == e.wipe ){
			if(CIRCLE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return false;
			}
			else if(CIRCLE_EVENT == this->eventType &&
				CIRCLE_EVENT == e.eventType)
			{
				return this->up->vertex->posY <= e.up->vertex->posY;
			}
			else if(SITE_EVENT == this->eventType &&
				SITE_EVENT == e.eventType)
			{
				return this->p->posY <= e.p->posY;
			}
			else
				return true;
		}
		else
			return this->wipe < e.wipe; 
	}

	bool operator==(const Event<T>& e) const  { 
		if( this->wipe == e.wipe ){
			if(this->eventType != e.eventType)
				return false;

			if(SITE_EVENT == this->eventType)
				return this->p == e.p;
			else
				return this->up == e.up && this->down == e.down;
		}
		else
			return false;
	}

	bool operator!=(const Event<T>& e) const  { return !(*this == e); }
};

//	 definition of voronoi
template<typename T>	//	T here is a numeric type [short, int, flowt, double, long ...]
class voronoi{
public:
	typedef point<T> _point;
	typedef Event<T> _event;
	typedef halfEdge<T> _halfEdge;
	typedef breakPoint<T> _breakPoint;
	typedef voronoi<T> _voronoi;

	typedef vector< _point > _pointList;
	typedef typename _pointList::iterator _pointList_iter;
	
	typedef redBlackTree< _halfEdge > _cell;
    typedef genericNode< _halfEdge >* _cell_iter;
	typedef map< _point *, _cell* > _cells;
	typedef typename map< _point *, _cell* >::iterator _cells_iter;
	
	typedef priorityQueue< _event > _eventQ;
	typedef genericNode< _event >* _eventQ_iter;

	typedef lockers< _point > _vertexList;
    typedef vector< _halfEdge* > _halfEdgeList;
    typedef typename _halfEdgeList::iterator _halfEdgeList_iter;
	
	typedef redBlackTree< _breakPoint > _beach;
	typedef genericNode< _breakPoint >* _beach_iter;

		

	/*
	*	properties
	*/
	T wipe;
	_pointList pointList;							//	store all the original points, do not participate in calculation directly
	_cells cells;	//	1-1 map from site to cell
	_eventQ eventQ;						//	site event & circle event
	_beach beach;					//	beach line
	_vertexList vertexList;							//	store all the vertices
    _halfEdgeList halfEdgeList;

	/*
	*	methods
	*/	
	const _voronoi* readPoints(const char*);
	const _voronoi* pushInSiteEvent();
	const _voronoi* initBeach();
	const _voronoi* updateAllBreakPoints();
	_event nextSiteEvent();
	_event nextCircleEvent();
	const _voronoi* handleNextSiteEvent();
	const _voronoi* handleNextCircleEvents();	//	conditions:	1. the out most event is circle event.	2. handle all circle events untile no more. 3. if new circle events occur, push in to eventQ.
	const _voronoi* extendBoundingBox();		//	conditions:	1. the eventQ is empty, 2. no more potential circle events	3. WIN_WIDTH & WIN_HEIGHT must be defined.
	const _voronoi* removeUpCircleEvent(_breakPoint*);
	const _voronoi* removeDownCircleEvent(_breakPoint*);
	const _voronoi* removeCircleEvent(_breakPoint*);
	void handleNormalSiteEvent(_event, _breakPoint*);
	void handleSpecialSiteEvent(_event, _breakPoint*);
	const _voronoi* checkNewCircleEvent(_breakPoint*, _breakPoint*); 

	/*
	*	constructors / destructors
	*/
	voronoi();
	~voronoi();
	const _voronoi* refresh();
	const _voronoi* clear();
   
   /*
    * draw vertices & edges    
    */
   void drawVertices(Display* dis, Window win, GC gc);
   void drawEdges(Display* dis, Window win, GC gc);
   void draw(Display* dis, Window win, GC gc);
};

template<typename T>
inline bool isInBox(T x, T y){
    return ( x >= 0 && x <= WIN_WIDTH && y >=0 && y <= WIN_HEIGHT );
}


//  code from wiki
OutCode ComputeOutCode(double x, double y)
{
    OutCode code;

    code = INSIDE;          // initialised as being inside of clip window

    if (x < 0)           // to the left of clip window
        code |= LEFT;
    else if (x > WIN_WIDTH)      // to the right of clip window
        code |= RIGHT;
    if (y < 0)           // below the clip window
        code |= BOTTOM;
    else if (y > WIN_HEIGHT)      // above the clip window
        code |= TOP;

    return code;
}

void CohenSutherlandLineClipAndDraw(double x0, double y0, double x1, double y1, Display* dis, Window win, GC gc)
{
    // compute outcodes for P0, P1, and whatever point lies outside the clip rectangle
    OutCode outcode0 = ComputeOutCode(x0, y0);
    OutCode outcode1 = ComputeOutCode(x1, y1);
    bool accept = false;
    
    double fx1 = x0, fy1 = y0, fx2 = x1, fy2 = y1;
    double x, y;

    while (true) {
        if (!(outcode0 | outcode1)) { // Bitwise OR is 0. Trivially accept and get out of loop
            accept = true;
            break;
        } else if (outcode0 & outcode1) { // Bitwise AND is not 0. Trivially reject and get out of loop
            break;
        } else {
            // failed both tests, so calculate the line segment to clip
            // from an outside point to an intersection with clip edge  
            

            // At least one endpoint is outside the clip rectangle; pick it.
            OutCode outcodeOut = outcode0? outcode0 : outcode1;

            // Now find the intersection point;
            // use formulas y = fy1 + slope * (x - x0), x = x0 + (1 / slope) * (y - fy1)
            if (outcodeOut & TOP) {           // point is above the clip rectangle
                x = fx1 + (fx2 - fx1) * (WIN_HEIGHT - fy1) / (fy2 - fy1);
                y = WIN_HEIGHT;
            } else if (outcodeOut & BOTTOM) { // point is below the clip rectangle
                x = fx1 + (fx2 - fx1) * (0 - fy1) / (fy2 - fy1);
                y = 0;
            } else if (outcodeOut & RIGHT) {  // point is to the right of clip rectangle
                y = fy1 + (fy2 - fy1) * (WIN_WIDTH - fx1) / (fx2 - fx1);
                x = WIN_WIDTH;
            } else if (outcodeOut & LEFT) {   // point is to the left of clip rectangle
                y = fy1 + (fy2 - fy1) * (0 - fx1) / (fx2 - fx1);
                x = 0;
            }

            //NOTE:*****************************************************************************************

            /* if you follow this algorithm exactly(at least for c#), then you will fall into an infinite loop 
            in case a line crosses more than two segments. to avoid that problem, leave out the last else
            if(outcodeOut & LEFT) and just make it else*/

            //**********************************************************************************************

            // Now we move outside point to intersection point to clip
            // and get ready for next pass.
            if (outcodeOut == outcode0) {
                fx1 = x;
                fy1 = y;
                outcode0 = ComputeOutCode(fx1, fy1);
            } else {
                fx2 = x;
                fy2 = y;
                outcode1 = ComputeOutCode(fx2, fy2);
            }
        }
    }
    if (accept) {           
        XDrawLine(dis, win, gc, (int)fx1, (int)fy1, (int)fx2, (int)fy2);
    }
}

template<typename T>
void voronoi<T>::drawEdges(Display* dis, Window win, GC gc){
    if(0 == this->cells.size()) return;
    
    point<T> ps, pe;
    int x1 = 0,
            y1 = 0,
            x2 = 0,
            y2 = 0;
    
    for(_halfEdgeList_iter iter = this->halfEdgeList.begin();
            iter != this->halfEdgeList.end();
            ++iter)
    {
        ps = *(*iter)->start;   ps = ps.trans();
        pe = *(*iter)->end;     pe = pe.trans();
        
        CohenSutherlandLineClipAndDraw(ps.posX, ps.posY, pe.posX, pe.posY, dis, win, gc);
    }
}

template<typename T>
void voronoi<T>::drawVertices(Display* dis, Window win, GC gc){
    if(0 == this->cells.size()) return;
    
    for(_pointList_iter iter = this->pointList.begin();
            iter != this->pointList.end();
            ++iter)
    {
        point<T> site = *iter;
        site = site.trans();
        
        XDrawArc(dis, win, gc, (int)site.posX, (int)site.posY, POINTSIZE, POINTSIZE, 0, 360*64);
        //XDrawLine(dis, win, gc, (int)site.posX, (int)site.posY, (int)site.posX+100, (int)site.posY);
    }
}

template<typename T>
void voronoi<T>::draw(Display* dis, Window win, GC gc){
    this->drawVertices(dis, win, gc);
    this->drawEdges(dis, win, gc);
}

template<typename T>
voronoi<T>::voronoi(){ this->eventQ.mode = MIN_PRIORITY; this->wipe = 0;}

template<typename T>
voronoi<T>::~voronoi(){
	this->clear();

	if(0 >= (int)this->cells.size()){
		for(_cells_iter iter = this->cells.begin();
			iter != cells.end();
			++ iter)
		{
			delete((*iter).second);
		}
	}
}

template<typename T>
const voronoi<T>* voronoi<T>::refresh(){
    this->halfEdgeList.clear();
    
	if(0 == this->pointList.size()) 
		return this;
	else {
		for(_pointList_iter iter = this->pointList.begin();
			iter != this->pointList.end();
			++ iter)
		{
			(*iter).move();
		}
	}

	if(0 >= (int)cells.size()){
		for(_cells_iter iter = this->cells.begin();
			iter != cells.end();
			++ iter)
		{
			if( iter->second->size != 0 )
				iter->second->clear();
		}
	}

	this->eventQ.clear();
	this->beach.clear();

	return this;
}

template<typename T>
const voronoi<T>* voronoi<T>::clear(){
	this->wipe = 0;

	if(0 != this->pointList.size())
		this->pointList.clear();

	if(0 != (int)cells.size()){
		for(_cells_iter iter = this->cells.begin();
			iter != cells.end();
			++ iter)
		{
			if(iter->second->size != 0)
				iter->second->clear();
		}
	}

	this->eventQ.clear();
	this->beach.clear();
    this->halfEdgeList.clear();

	return this;
}
	
template<typename T>
const voronoi<T>* voronoi<T>::readPoints(const char* pointsFile){ 
	/*
	*	step1 read points from a txt file, and store them in a list.
	*	we don't care about the order.
	*/
	FILE *handle = fopen(pointsFile, "r");

	if(NULL == handle) {
		std::cerr<<"Failed to open file"<<endl;
	}

	char buffer[1024];
	int a = 0, b = 0, c = 0, d = 0;

	while(fgets(buffer, 1024, handle)){
		sscanf(buffer, "%d,%d,%d,%d\n", &a, &b, &c, &d);
		this->pointList.push_back(_point(a, b, c, d));
	}
	sort(pointList.begin(), pointList.end());

	fclose(handle);

	return this; 
}

	
template<typename T>
const voronoi<T>* voronoi<T>::pushInSiteEvent(){
	if(0 == this->pointList.size()) return this;
	if(0 != this->eventQ.size) this->eventQ.clear();

	_point* tmpPoint = NULL;

	for(_pointList_iter iter = this->pointList.begin();
		iter != this->pointList.end();
		iter ++)
	{
		if( iter == this->pointList.begin() ){
			tmpPoint = (this->eventQ.insert( _event(SITE_EVENT, iter->posX, &(*iter)) ))->key.p;

			if(0 != this->cells[tmpPoint])
				this->cells[tmpPoint]->clear();
			else
				this->cells[tmpPoint] = new _cell();
		}
		else{
			if(iter->posX == (iter-1)->posX && iter->posY == (iter-1)->posY)
				continue;
			else{
				tmpPoint = (this->eventQ.insert( _event(SITE_EVENT, iter->posX, &(*iter)) ))->key.p;					

				if(0 != this->cells[tmpPoint])
					this->cells[tmpPoint]->clear();
				else
					this->cells[tmpPoint] = new _cell();
			}
		}			
	}

	tmpPoint = NULL;

	return this;
}	

template<typename T>
const voronoi<T>* voronoi<T>::initBeach(){
	if(2 > this->eventQ.size) return this;

	_point *p1 = this->eventQ.pop().p,
		*p2 = this->eventQ.pop().p;

	this->wipe = p2->posX;

	if(p1->posX != p2->posX){
		//	1st break point
		_point* v1 = this->vertexList.deposit(_point());
		_breakPoint bp1 = _breakPoint(v1, p1, p2);
		bp1.updateCoordinates(this->wipe);
		this->beach.insert( bp1 );
			
		//	2nd break point
		_point* v2 = this->vertexList.deposit(_point());
		_breakPoint bp2 = _breakPoint(v2, p2, p1);
		bp2.updateCoordinates(this->wipe);
		this->beach.insert( bp2 );
			
			
		//	2 half edges
		_halfEdge he1(p1, v2, v1),
			he2(p2, v1, v2);	// !! could be a bug

		/*	since the address is in stack segment, so it's not real.	*/
		he1.twin = &he2;	//	setting twin is critical for calculating key
		he2.twin = &he1;
		/*	end	*/
			
		he1.calcKey();		//	use a temporary halfEdge to calculate the true key, then later, we will update the twin property
		he2.calcKey();	
		//	put the edges into cells

		_halfEdge * realHe1 = &this->cells[p1]->insert(he1)->key;
		_halfEdge * realHe2 = &this->cells[p2]->insert(he2)->key;
        
        if(0 <= realHe1->key && 180.0 > realHe1->key)
            this->halfEdgeList.push_back(realHe1);
        else
            this->halfEdgeList.push_back(realHe2);
			
		realHe1->twin = realHe2;
		realHe2->twin = realHe1;

		v1 = NULL; v2 = NULL;
		realHe1 = NULL; realHe2 = NULL;
	}
	//	todo: else clause
	else{
		//	use p1 & p2 as 2 stubs
		//	we can read on, and pass the p2 value to p1, then point p2 to the new site point.
		//	then do the same thing.
		do{
			_point* v1 = this->vertexList.deposit(_point());
			_point* v2 = this->vertexList.deposit(_point());	//	-1, (p1->posY + p2->posY)/2		//	x coordinate could be -1, simply				
				
			_breakPoint bp(v2, p2, p1);
			bp.updateCoordinates(this->wipe);
			this->beach.insert( _breakPoint(v2, p2, p1) );
				
			*v1 = *v2;
				
			_halfEdge he1(p1, v2, v1),
				he2(p2, v1, v2);

			he1.twin = &he2;
			he2.twin = &he1;

			he1.calcKey();
			he2.calcKey();
				
			_halfEdge * realHe1 = &this->cells[p1]->insert(he1)->key;
			_halfEdge * realHe2 = &this->cells[p2]->insert(he2)->key;

			realHe1->twin = realHe2;
			realHe2->twin = realHe1;
            
            if(0 <= realHe1->key && 180.0 > realHe1->key)
                this->halfEdgeList.push_back(realHe1);
            else
                this->halfEdgeList.push_back(realHe2);
				
			if(0 == this->eventQ.size)
				break;
			else if(this->wipe == this->eventQ.front().p->posX){
				p1 = p2;
				p2 = this->eventQ.pop().p;
			}
			else
				break;

			v1 = NULL; v2 = NULL;
			realHe1 = NULL; realHe2 = NULL;

		}while(true);
	}

	p1 = NULL;
	p2 = NULL;

	return this; 
}

template<typename T>
const voronoi<T>* voronoi<T>::updateAllBreakPoints(){
	if(0 == this->beach.size) return this;

	for(int i=0; i<this->beach.size; i++){
		this->beach[i].updateCoordinates(this->wipe);
	}

	return this;
}

template<typename T>
Event<T> voronoi<T>::nextSiteEvent(){
	if(0 == this->eventQ.size) return Event<T>(NIL_EVENT, 0);

	if(SITE_EVENT == this->eventQ.front().eventType) return this->eventQ.front();

	for(_eventQ_iter iter = this->eventQ.begin();
		iter != this->eventQ.end();
		iter = iter->next())
	{
		if(SITE_EVENT == iter->key.eventType)
			return iter->key;
	}

	return _event(NIL_EVENT, 0);
}

template<typename T>
Event<T> voronoi<T>::nextCircleEvent(){
	if(0 == this->eventQ.size) return Event<T>(NIL_EVENT, 0);

	if(CIRCLE_EVENT == this->eventQ.front().eventType) return this->eventQ.front();

	for(_eventQ_iter iter = this->eventQ.begin();
		iter != this->eventQ.end();
		iter = iter->next())
	{
		if(CIRCLE_EVENT == iter->key.eventType)
			return iter->key;
	}

	return _event(NIL_EVENT, 0);
}

template<typename T>
const voronoi<T>* voronoi<T>::checkNewCircleEvent(breakPoint<T>* up, breakPoint<T>* down){
	if(up->downSite != down->upSite) return this;
		
	_point *p1 = up->upSite,
		*p2 = up->downSite,
		*p3 = down->downSite;

	if(CCW == p1->isCCW(p2, p3)){
		T wipe = getWipeX(p1->posX, p1->posY, p2->posX, p2->posY, p3->posX, p3->posY);
		Event<T> e( CIRCLE_EVENT, wipe, up, down );
		this->eventQ.insert( e );
	}

	p1 = NULL; p2 = NULL; p3 = NULL;

	return this;
}

template<typename T>
void voronoi<T>::handleNormalSiteEvent(Event<T> e, breakPoint<T>* mark){
	_point* p1 = NULL;
	_point* p2 = e.p;

	if(e.p->posY < mark->vertex->posY)
		p1 = mark->downSite;
	else
		p1 = mark->upSite;

	//	1st break point
	_point* v1 = this->vertexList.deposit(_point());
	_breakPoint bp1 = _breakPoint(v1, p1, p2);
	bp1.updateCoordinates(this->wipe);
	_breakPoint* bpPtr1 = &this->beach.insert(bp1)->key;
		
			
	//	2nd break point
	_point* v2 = this->vertexList.deposit(_point());
	_breakPoint bp2 = _breakPoint(v2, p2, p1);
	bp2.updateCoordinates(this->wipe);
	_breakPoint* bpPtr2 = &this->beach.insert(bp2)->key;
			
	//	2 half edges
	_halfEdge he1(p1, v2, v1),
		he2(p2, v1, v2);

	he1.twin = &he2;	//	setting twin is critical for calculating key
	he2.twin = &he1;
			
	he1.calcKey();	
	he2.calcKey();	

	//	put the edges into cells
	_halfEdge* realHe1 = &this->cells[p1]->insert(he1)->key;
	_halfEdge* realHe2 = &this->cells[p2]->insert(he2)->key;
    
    if(0 <= realHe1->key && 180 > realHe1->key)
        this->halfEdgeList.push_back(realHe1);
    else
        this->halfEdgeList.push_back(realHe2);

	realHe1->twin = realHe2;
	realHe2->twin = realHe1;

	_breakPoint* bpPtr3 = 0;
		
	_beach_iter tmpBkNode = this->beach.successor( this->beach.search( *bpPtr1, this->beach.root ) );
	if(this->beach.NIL != tmpBkNode){			
		bpPtr3 = &tmpBkNode->key;

		this->checkNewCircleEvent(bpPtr3, bpPtr1);
	}

	tmpBkNode = this->beach.predecessor( this->beach.search( *bpPtr2, this->beach.root ) );
	if(this->beach.NIL != tmpBkNode){
		bpPtr3 = &tmpBkNode->key;		

		this->checkNewCircleEvent(bpPtr2, bpPtr3);
	}	

	p1 = NULL; p2 = NULL;
	v1 = NULL; v2 = NULL;
	bpPtr1 = NULL; bpPtr2 = NULL; bpPtr3 = NULL;
	realHe1 = NULL; realHe2 = NULL;
}

template<typename T>
void voronoi<T>::handleSpecialSiteEvent(Event<T> e, breakPoint<T>* mark){
	_point	*p3 = e.p,				//	*p3 
			*p1 = mark->downSite,
			*p2 = mark->upSite;

	_point	*v1 = this->vertexList.deposit( *mark->vertex ),	//	*v1 is the fixed vertex
			*v2 = this->vertexList.deposit( *mark->vertex ),			//	*v2 is the down bound vertex
			*v3 = this->vertexList.deposit( *mark->vertex );			//	*v3 is the up bound vertex

	_halfEdge he11(p1, v2, v1),
		he12(p3, v1, v2),
		he21(p2, v1, v3),
		he22(p3, v3, v1);

	he11.twin = &he12;	he12.twin = &he11;
	he21.twin = &he22;	he22.twin = &he21;

	he11.calcKey(); he12.calcKey();
	he21.calcKey(); he22.calcKey();

	_halfEdge *realHe11 = &this->cells[p1]->insert(he11)->key,
		*realHe12 = &this->cells[p3]->insert(he12)->key,
		*realHe21 = &this->cells[p2]->insert(he21)->key,
		*realHe22 = &this->cells[p3]->insert(he22)->key;
    
    if(0 <= realHe11->key && 180 > realHe11->key)
        this->halfEdgeList.push_back(realHe11);
    else
        this->halfEdgeList.push_back(realHe12);
    
    if(0 <= realHe21->key && 180 > realHe21->key)
        this->halfEdgeList.push_back(realHe21);
    else
        this->halfEdgeList.push_back(realHe22);

	realHe11->twin = realHe12; realHe12->twin = realHe11;
	realHe21->twin = realHe22; realHe22->twin = realHe21;

	breakPoint<T> bp1(v2, p3, p1),
		bp2(v3, p2, p3);

	bp1.updateCoordinates(this->wipe);
	bp2.updateCoordinates(this->wipe);

	this->beach.remove(*mark);
	this->beach.insert(bp1);
	this->beach.insert(bp2);

	p1 = NULL; p2 = NULL; p3 = NULL;
	v1 = NULL; v2 = NULL; v3 = NULL;
	realHe11 = NULL; realHe12 = NULL;
	realHe21 = NULL; realHe22 = NULL;
}

template<typename T>
const voronoi<T>* voronoi<T>::handleNextSiteEvent(){
	if(0 == this->eventQ.size) return this;
	if(0 == this->beach.size) return this;

	if(SITE_EVENT != this->eventQ.front().eventType) return this;
		
	_event e = this->eventQ.pop();

	bool hitABreakPoint = false;
	this->wipe = e.p->posX;
	T y = e.p->posY;

	this->updateAllBreakPoints();

	_beach_iter iter = this->beach.root,
		mark = iter;

	while(iter != this->beach.NIL){
		mark = iter;
			
		if(y == iter->key.vertex->posY){
			hitABreakPoint = true;
			break;
		}
		if(y < iter->key.vertex->posY)
			iter = iter->left;
		else
			iter = iter->right;
	}
	
	if(!hitABreakPoint){
		this->handleNormalSiteEvent(e, &mark->key);
	}
	else{	//	this clause : when a site event hits a break point.	//	we should notice that, no more than one site event hit a single break point.	We can prove this.
		this->handleSpecialSiteEvent(e, &mark->key);
	}

	iter = NULL;
	mark = NULL;

	return this;
}

template<typename T>
const voronoi<T>* voronoi<T>::removeUpCircleEvent(breakPoint<T>* bk){
	if(0 == this->beach.search(*bk, this->beach.root)) return this;
		
	if( bk == &this->beach.begin()->key ) return this;

	if(0 == this->beach.search(*bk, this->beach.root)) return this;

	_beach_iter iter = this->beach.successor( this->beach.search(*bk, this->beach.root) );

	if(this->beach.NIL == iter) return this;

	_breakPoint *up = &iter->key;

	if( CCW == up->upSite->isCCW(up->downSite, bk->downSite) ){
		T wipe = getWipeX(up->upSite->posX, up->upSite->posY, up->downSite->posX, up->downSite->posY, bk->downSite->posX, bk->downSite->posY);
		this->eventQ.remove( Event<T>(CIRCLE_EVENT, wipe, up, bk) );
	}

	up = NULL;
	iter = NULL;

	return this;
}

template<typename T>
const voronoi<T>* voronoi<T>::removeDownCircleEvent(breakPoint<T>* bk){
	if(0 == this->beach.search(*bk, this->beach.root)) return this;

	if( bk == &this->beach.begin()->key ) return this;

	if( 0 == this->beach.search(*bk, this->beach.root) ) return this;

	_beach_iter iter = this->beach.predecessor( this->beach.search(*bk, this->beach.root) );

	if( this->beach.NIL == iter ) return this;
			
	breakPoint<T> *down = &iter->key;

	if( CCW == bk->upSite->isCCW(bk->downSite, down->downSite) ){
		T wipe = getWipeX(bk->upSite->posX, bk->upSite->posY, bk->downSite->posX, bk->downSite->posY, down->downSite->posX, down->downSite->posY);
		this->eventQ.remove( Event<T>(CIRCLE_EVENT, wipe, bk, down) );
	}

	iter = NULL;
	down = NULL;

	return this;
}

template<typename T>
const voronoi<T>* voronoi<T>::removeCircleEvent(breakPoint<T>* bk){
	this->removeUpCircleEvent(bk);
	this->removeDownCircleEvent(bk);
	return this;
}

template<typename T>
const voronoi<T>* voronoi<T>::handleNextCircleEvents(){	//	no exhausting, need fix, could be a bug
	if(0 == this->eventQ.size) return this;

	while(0 != this->eventQ.size ){	
		if(CIRCLE_EVENT != this->eventQ.front().eventType) break;

		_event e = this->eventQ.pop();
      if(0 == this->beach.search(*e.up)) continue;
      if(0 == this->beach.search(*e.down)) continue;

		//	update wipe
		this->wipe = e.wipe;
		this->updateAllBreakPoints();

		_breakPoint *up = e.up,
			*down = e.down;
      
		//	we need to detect 2 circle events and delete them
		this->removeUpCircleEvent(up);
		this->removeDownCircleEvent(down);			
        
		//	add 2 new vertex ( we don't delete vertex )	& we don't need to care about them anymore.
		_point *newpStart = this->vertexList.deposit( *up->vertex ),	
			*newpEnd = this->vertexList.deposit( *up->vertex ),
			*upS = up->upSite,
			*downS = down->downSite;	

		//	since break points are "magnetic heads", so if we don't remove them, they will keep going.
		_beach_iter tmpBkNode = this->beach.nextContainer(*up);	 //this->beach.successor( this->beach.search(*up, this->beach.root) );	
        
		if(this->beach.NIL != tmpBkNode && abs(newpEnd->posY - tmpBkNode->key.vertex->posY)<ZERO){          
			_breakPoint* currentBk = &tmpBkNode->key;
			_breakPoint* nextBk = currentBk;            
         
			this->beach.remove( *up );
			this->beach.remove( *down );

			while(true){	
				currentBk = nextBk;
				if(0 != this->beach.search( *currentBk, this->beach.root )){
					if(abs(newpEnd->posX - currentBk->vertex->posX)>ZERO || abs(newpEnd->posY - currentBk->vertex->posY)>ZERO)
						break;
					else{
						upS = currentBk->upSite;
						tmpBkNode = this->beach.nextContainer(*nextBk);
							
						this->removeCircleEvent( currentBk );
						this->beach.remove( *currentBk );

						if(this->beach.NIL == tmpBkNode)
							break;
						else
							nextBk = &tmpBkNode->key;
					}
				}
				else
					break;
			}

			currentBk = NULL;
			nextBk = NULL;
		}
		else{				
			this->beach.remove( *up );
			this->beach.remove( *down );
		}
        
		//	search for a site event which hit the intersection break point.
		if(0 != this->eventQ.size){
			_eventQ_iter eIter = this->eventQ.root;
			bool siteEventFound = false;

			while(this->eventQ.NIL != eIter){	//	bug
				//cout<<"trap2"<<endl;
				if(eIter->key.eventType == SITE_EVENT && eIter->key.wipe == this->wipe && eIter->key.p->posY == newpEnd->posY){
					siteEventFound = true;
					break;
				}
				else if(eIter->key.wipe > this->wipe){
					//cout<<"left"<<endl;
					eIter = eIter->left;
				}
				else{
					//cout<<"right"<<endl;
					eIter = eIter->right;
				}
			}

			if(siteEventFound){					
				_event tmpEvent = eIter->key;
				this->eventQ.remove(eIter);

				_breakPoint *newBk = &this->beach.insert( breakPoint<T>(newpEnd, upS, downS) )->key; 
				this->handleSpecialSiteEvent(tmpEvent, newBk);

				newBk = NULL;

				return this;
			}

			eIter = NULL;
		}
      
      

		//	add 2 new halfEdges
		_halfEdge he1 = _halfEdge(upS, newpStart, newpEnd),
			he2 = _halfEdge(downS, newpEnd, newpStart);

		he1.twin = &he2;
		he2.twin = &he1;

		he1.calcKey();
		he2.calcKey();
			
		_halfEdge *realHe1 = &this->cells[upS]->insert( he1 )->key,
			*realHe2 = &this->cells[downS]->insert( he2 )->key;
        
        if(0 <= realHe1->key && 180 > realHe1->key)
            this->halfEdgeList.push_back(realHe1);
        else
            this->halfEdgeList.push_back(realHe2);

		//	this step is required.
		realHe1->twin = realHe2;
		realHe2->twin = realHe1;

		//	check whether new event is created.
		_point *bkP1 =0, *bkP2 = 0, *bkP3 = 0;

		_breakPoint *newBk = &this->beach.insert( breakPoint<T>(newpEnd, upS, downS) )->key;			

		_beach_iter tmpBkNode1 = this->beach.successor( this->beach.search( *newBk, this->beach.root ) );
		if(this->beach.NIL != tmpBkNode1 && 0 != tmpBkNode1){
			_breakPoint *newBkUp = &tmpBkNode1->key;

			bkP1 = newBkUp->upSite,
			bkP2 = newBkUp->downSite,
			bkP3 = newBk->downSite;			
				
			if(CCW == bkP1->isCCW(bkP2, bkP3)){
                T newWipe1 = getWipeX(bkP1->posX, bkP1->posY, bkP2->posX, bkP2->posY, bkP3->posX, bkP3->posY);
                
				Event<T> tmpEvent(CIRCLE_EVENT, newWipe1, newBkUp, newBk);		//Event<T>(CIRCLE_EVENT, newWipe1, newBkUp, newBk)

				this->eventQ.insert( tmpEvent );
			}

			newBkUp = NULL;
		}

		_beach_iter tmpBkNode2 = this->beach.predecessor( this->beach.search( *newBk, this->beach.root ) );
		if(this->beach.NIL != tmpBkNode2 && 0 != tmpBkNode2){
			_breakPoint *newBkDown = &tmpBkNode2->key;

			bkP1 = newBk->upSite;
			bkP2 = newBk->downSite;
			bkP3 = newBkDown->downSite;
			
			if(CCW == bkP1->isCCW(bkP2, bkP3)){
                T newWipe2 = getWipeX(bkP1->posX, bkP1->posY, bkP2->posX, bkP2->posY, bkP3->posX, bkP3->posY);
                
				Event<T> tmpEvent(CIRCLE_EVENT, newWipe2, newBk, newBkDown);	//Event<T>(CIRCLE_EVENT, newWipe2, newBk, newBkDown)
					
				this->eventQ.insert( tmpEvent );
			}

			newBkDown = NULL;
		}

		up = NULL;
		down = NULL;
		newpStart = NULL;
		newpEnd = NULL;
		upS = NULL;
		downS = NULL;
		tmpBkNode = NULL;
		realHe1 = NULL;
		realHe2 = NULL;
		bkP1 = NULL;
		bkP2 = NULL;
		bkP3 = NULL;
		newBk = NULL;
		tmpBkNode1 = NULL;
		tmpBkNode2 = NULL;
	}
		
	return this;
}

template<typename T>
const voronoi<T>* voronoi<T>::extendBoundingBox(){
	if(0 != this->eventQ.size) return this;

	if(0 == this->beach.size) return this;

	T wipe = this->wipe,
		tmpWipe = wipe;

	for(_beach_iter iter = this->beach.begin();
		iter != this->beach.NIL;
		iter = iter->next())
	{
		tmpWipe = getBoundingWipe(iter->key.upSite, iter->key.downSite);
		if(wipe < tmpWipe)
			wipe = tmpWipe;
	}

	for(_beach_iter iter = this->beach.begin();
		iter != this->beach.NIL;
		iter = iter->next())
	{
        iter->key.updateCoordinates(wipe);
	}

	return this;
}

//  used only for rendering
unsigned long GetColor( Display* dis, char* color_name )
{
    Colormap cmap;
    XColor near_color, true_color;
 
    cmap = DefaultColormap( dis, 0 );
    XAllocNamedColor( dis, cmap, color_name, &near_color, &true_color );
    return( near_color.pixel );
}

void switchPen(char* color){
        XSetForeground( dis, gc, BlackPixel(dis, 0)^GetColor( dis, color));
}

//	main function
bool static_act = false;

int main(int argc, char** argv){
    //  display part
    
    //   voronoi part
    voronoi<double> vrn;
    vrn.readPoints("points.txt"); 
    
    if(static_act){
        vrn.pushInSiteEvent();
        vrn.initBeach();

        while(0 != vrn.eventQ.size){
          vrn.handleNextSiteEvent();
          vrn.handleNextCircleEvents();          
        }

        vrn.extendBoundingBox();
    }

    //   rendering
    dis = XOpenDisplay( NULL );
    win = XCreateSimpleWindow( dis, RootWindow(dis,0), 100, 100,
      WIN_WIDTH, WIN_HEIGHT, 5, WhitePixel(dis,0), BlackPixel(dis,0) );

    att.backing_store = WhenMapped;
    XChangeWindowAttributes( dis, win, CWBackingStore, &att );

    XSelectInput( dis, win, ExposureMask );
    XMapWindow( dis, win );

    do{
        XNextEvent( dis, &ev);
    }while( ev.type != Expose );
    
    gc = XCreateGC( dis, DefaultRootWindow(dis), 0, 0 );    
    XSetLineAttributes( dis, gc, POINTSIZE, LineSolid, CapRound, JoinRound);
    XSetFunction( dis, gc, GXxor );
    
    lineGc = XCreateGC( dis, DefaultRootWindow(dis), 0, 0 );
    XSetLineAttributes( dis, lineGc, 1, LineSolid, CapRound, JoinRound);
    XSetFunction( dis, lineGc, GXxor );

    int i = 20, j = 0;
    while(1)
    {        
        if(!static_act){
            vrn.refresh();
            vrn.pushInSiteEvent();
            vrn.initBeach();

            while(0 != vrn.eventQ.size){
              vrn.handleNextSiteEvent();
              vrn.handleNextCircleEvents();          
            }

            vrn.extendBoundingBox();
        }
    
        XSetForeground( dis, gc, BlackPixel(dis, 0)^GetColor( dis, "red"));
        XSetForeground( dis, lineGc, BlackPixel(dis, 0)^GetColor( dis, "green"));
        //vrn.draw(dis, win, lineGc);
        vrn.drawVertices(dis, win, gc);
        vrn.drawEdges(dis, win, lineGc);
        
        
        XSetForeground( dis, gc, BlackPixel(dis, 0)^GetColor( dis, "red"));
        XSetForeground( dis, lineGc, BlackPixel(dis, 0)^GetColor( dis, "green"));

        usleep(60000);
        //vrn.draw(dis, win, lineGc);
        vrn.drawVertices(dis, win, gc);
        vrn.drawEdges(dis, win, lineGc);
    }
    
    getchar(); 
    XDestroyWindow( dis , win );
    XCloseDisplay( dis );
    
    getchar();
	return EXIT_SUCCESS;   
}
